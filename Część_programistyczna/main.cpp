#include <iostream>
#include <vector>
#include <list>
#include <climits>
#include <fstream>
#include <cstdio>

using namespace std;

int min(int a, int b)
{
    return a < b ? a : b;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

class Edge
{
public:
    int vertex;
    int capacity;
    int current_flow;
    Edge(int ver, int cap): vertex(ver), capacity(cap), current_flow(0) {}
};

class Graph
{
public:
    vector<vector<Edge>> adjecency_list;
    Graph(int num_of_ver): adjecency_list(num_of_ver)
    {}
    void add_edge(int beg, int end, int cap)
    {
        Edge* edge = new Edge(end, cap);
        adjecency_list[beg].push_back(*edge);
    }
    int get_remaining_capacity(int ver1, int ver2)
    {
        for (auto &edge: adjecency_list[ver1]) {
            if (edge.vertex == ver2) return edge.capacity - edge.current_flow;
        }
        return  0;
    }
    void update_flow(int ver1, int ver2, int flow)
    {
        for (auto &edge: adjecency_list[ver1]) {
            if (edge.vertex == ver2)
            {
                edge.current_flow += flow;
            }
        }
    }
    bool BFS(int parents[])
    {
        int vnum = adjecency_list.size();
        int source = 0;
        int sink =  vnum - 1;


        bool *visited = new bool[vnum];
        for(int i = 0; i < vnum; i++)
            visited[i] = false;

        list<int> queue;

        visited[source] = true;
        parents[source] = -1;
        queue.push_back(source);
        int vertex;

        while(!queue.empty())
        {
            vertex = queue.front();
//            cout << vertex << endl;
            queue.pop_front();

            for (auto &edge: adjecency_list[vertex])
            {
                if (!visited[edge.vertex] && edge.capacity-edge.current_flow > 0)
                {
                    parents[edge.vertex] = vertex;
                    if(edge.vertex == sink) return true;

                    visited[edge.vertex] = true;
                    queue.push_back(edge.vertex);
                }
            }
        }
        return false;
    }
    int edmonts_karp()
    {
        int maximum_flow = 0;
        int size = adjecency_list.size();
        int *parents = new int(size);
        int *child = new int(size);
        int source = 0;
        int sink = size - 1;
        while(BFS(parents))
        {
            int path_flow = INT_MAX;
            for (int t = sink; t != source; t = parents[t]) {
                child[parents[t]] = t;
                path_flow = min(path_flow, get_remaining_capacity(parents[t], t));
            }
            for (int t = sink; t != source; t = parents[t]) {
                update_flow(parents[t],t,path_flow);
            }
            maximum_flow += path_flow;

            for (int i = source; i != sink; i = child[i]) {
                cout << i << " --->>> ";
            }
            cout << sink << "   flow: " << path_flow << endl;
        }
        return maximum_flow;
    }
};

int main()
{
    int highest_v = 0;
    ifstream file;
    file.open("graph.json");
    char word[50];
    char c;
    string line;
    int in, out, weight, gsize = -1;
    while((c = file.get()) != EOF)
    {
        if (c == '[') ++gsize;
    }

    file.clear();
    file.seekg(0, ios::beg);

    // cout << gsize << endl;

    Graph* graph = new Graph(gsize);

    while ( (c = file.get()) != '[');
    while( (c = file.get()) != EOF)
    {
        if(c == '[')
        {
            while( (c = file.get()) != ']')
            {
                if(c == ' ') continue;
                line.push_back(c);
            }
            // cout << line << endl;
            const char *c = line.c_str();
            sscanf(c,"%i,%i,%i",&in,&out,&weight);
            // cout << in << ' ' << out << ' ' << weight << endl;
            graph->add_edge(in,out,weight);
            highest_v = max(highest_v, out);
            line.clear();
        }
    }
    graph->adjecency_list.resize(highest_v + 1);

    int max = graph->edmonts_karp();

    cout << endl << "Source: " << 0 << endl << "Sink: " << highest_v << endl << endl;

    int i = 0;
    for (auto &edge: graph->adjecency_list) {
        if (i == highest_v) break;
        cout << "From vertex: " << i << endl;
        ++i;
        for (auto e: edge) {
            cout << "To vertex: " << e.vertex << ", flow: " << e.current_flow << ", capacity: " << e.capacity << endl;
        }
        cout << endl;
    }

    cout << "Maximum flow is: " << max << endl;

    return 0;
}




