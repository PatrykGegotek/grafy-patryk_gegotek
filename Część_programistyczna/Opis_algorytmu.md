# **Metoda Forda-Fulkersona**

Metoda ta służy do rozwiązywania wielu różnych problemów związanych z ogólnie pojętnym przepływem.
Może służyć do:
1. Obliczenia przepływu cieczy przez połączone rury, umożliwiające przepływ jednostronny, na podstawie ich wielkości (czyli pojemności).
2. Stwierdzenie przepustowości dróg w mieście, gdy chcemy dostać się z punktu A do punktu B, biorąc pod uwagę kierunek dróg oraz ich indywidualnych przepustowości.
3. Obliczenia natężenia prądu płynącego przez rozwidlone kable.
