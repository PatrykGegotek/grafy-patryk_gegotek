# **Algorytm Edmondsa-Karpa**

Algorytm zaimplementowany jest w języku C++.
Aby go uruchomić, należy użyć komendy *g++ main.cpp* i następnie uruchomić plik poleceniem *./a.exe* lub *./a.out*

Graf znajduje się w pliku *graph.json*. Poprawnym zapisem danego grafu jest macierz dwuwymiarowa, zawierająca dane krawędzie,
a one zaś informacje o:
1. Wierzchołku, z którego krawędź wychodzi
2. Wierzchołku do którego wchodzi
3. Pojemność krawędzi

w podanej kolejności, gdzie odpowiednie dane i krawędzie oddzielone są przecinkami, np
**[[0,1,9],[1,2,8]]** oznacza dwie krawędzie, jedna wychodzi z wierzchołka 0, wchodzi do 1, a jej pojemność wynosi 9, druga wychodzi z 1, wchodzi do 2, jej pojemność to 8.

Wierzchołek początkowy (source) **musi** mieć wartość 0, zaś końcowy (sink) najwiekszą wartość ze wszystkich wierzchołków, a pojemności muszą być dodatnimi liczbami naturalnymi.
